@tag
Feature: Employee CRUD Functionalities
  As a user I want to CRUD data on the database

  @tag1
  Scenario: Add employee on the database
    Given Employee has firstName
    And Employee has lastName
    And Employee has department
    And Employee has position
    When I create employee add data
    Then Add employee data

    @tag2
    Scenario: Update employee on the database
      Given Employee has Id
      And Employee has firstName
      And Employee has lastName
      And Employee has department
      And Employee has position
      When Employee to update is existing
      And I create employee update data
      Then Update employee data

      @tag3
      Scenario: Delete data on the database by Id
        Given Employee has Id
        And Employee has firstName
        And Employee has lastName
        And Employee has department
        And Employee has position
        When Employee to delete is existing
        Then Delete employee data

        @tag4
        Scenario: Read all data on the database
          Given Employee has Id
          And Employee has firstName
          And Employee has lastName
          And Employee has department
          And Employee has position
          When Employee to read is existing
          Then Return all data

          @tag5
          Scenario: Read data on the database by Id
            Given Employee has Id
            And Employee has firstName
            And Employee has lastName
            And Employee has department
            And Employee has position
            When Employee to read by Id is existing
            Then Return employee by Id
