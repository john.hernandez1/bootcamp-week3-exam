package com.novare.bootcamp.week3.employeeprofile.service;

import com.novare.bootcamp.week3.employeeprofile.model.Employee;

import java.util.List;

public interface EmployeeService {

    Employee addEmployee(Employee employee);

    Employee getEmployee(long id) throws Exception;

    List<Employee> getAll();

    Employee updateEmployee(Employee employee) throws Exception;

    long deleteEmployee(long id) throws Exception;

    boolean getProcess();
}

