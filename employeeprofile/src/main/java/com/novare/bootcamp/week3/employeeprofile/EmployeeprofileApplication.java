package com.novare.bootcamp.week3.employeeprofile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeeprofileApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeeprofileApplication.class, args);
	}

}
