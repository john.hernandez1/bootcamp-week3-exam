package com.novare.bootcamp.week3.employeeprofile.controller;

import com.novare.bootcamp.week3.employeeprofile.model.Employee;
import com.novare.bootcamp.week3.employeeprofile.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;


    @GetMapping("/get/{id}")
    public Employee getEmployee(@PathVariable long id) throws Exception{
        return employeeService.getEmployee(id);
    }
    @GetMapping("/get/all")
    public List<Employee> getAllEmployee(){
        return employeeService.getAll();
    }

    @PostMapping(value = "/add/{firstName}/{lastName}/{department}/{position}")
    public Employee addEmployee(@PathVariable String firstName,
                                @PathVariable String lastName,
                                @PathVariable String department,
                                 @PathVariable String position) {
        return employeeService.addEmployee(new Employee(firstName,lastName,department,position));
    }

    @PutMapping("/update")
    public Employee updateEmployee(@RequestBody Employee employee) throws Exception {
        return employeeService.updateEmployee(employee);
    }

    @DeleteMapping("/delete/{id}")
    public long deleteEmployee(@PathVariable("id") long id) throws Exception{
        employeeService.deleteEmployee(id);
        return id;
    }

    @GetMapping("/upload")
    public void getVoid(){
        employeeService.getProcess();
    }

}
