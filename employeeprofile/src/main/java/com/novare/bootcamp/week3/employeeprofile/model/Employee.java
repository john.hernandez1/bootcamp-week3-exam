package com.novare.bootcamp.week3.employeeprofile.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonTypeId;


import javax.persistence.*;
import java.util.Date;

@Entity
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String firstName;
    private String lastName;
    private Date dateHired;
    private String department;
    private String position;

    //@JsonManagedReference
    @ManyToOne
    @JoinColumn(name = "adminManagerId")
    private Manager manager;

    public Employee() {

    }

    public Employee(String firstName,
                    String lastName,
                    String department,
                    String position){
        this.firstName = firstName;
        this.lastName = lastName;
        this.department = department;
        this.position = position;
        this.dateHired = new Date();
    }

    public Employee(long id,
                    String firstName,
                    String lastName,
                    String department,
                    String position){
        this.firstName = firstName;
        this.lastName = lastName;
        this.department = department;
        this.position = position;
        this.dateHired = new Date();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getDateHired() {
        return dateHired;
    }

    public void setDateHired(Date dateHired) {
        this.dateHired = dateHired;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Manager getManager() {
        return manager;
    }

    public void setManager(Manager manager) {
        this.manager = manager;
    }

    public boolean hasFirstName() {
        if(this.getFirstName()!=null) {
            return true;
        }
        else {
            return false;
        }
    }

    public boolean hasLastName() {
        if(this.getLastName()!=null) {
            return true;
        }
        else {
            return false;
        }
    }

    public boolean hasDepartment() {
        if(this.getDepartment()!=null) {
            return true;
        }
        else{
            return false;
        }
    }

    public boolean hasPosition() {
        if(this.getPosition()!=null) {
            return true;
        }
        else {
            return false;
        }
    }

    public boolean isValid() {
        return this.hasFirstName() && this.hasLastName() && this.hasDepartment() && this.hasPosition();
    }

}
