package com.novare.bootcamp.week3.employeeprofile.dao;

import com.novare.bootcamp.week3.employeeprofile.model.Manager;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ManagerDao extends JpaRepository<Manager, Long> {
}
