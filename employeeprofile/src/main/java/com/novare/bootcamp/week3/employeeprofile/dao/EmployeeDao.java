package com.novare.bootcamp.week3.employeeprofile.dao;

import com.novare.bootcamp.week3.employeeprofile.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeDao extends JpaRepository<Employee, Long> {
}
