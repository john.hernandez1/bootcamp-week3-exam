package com.novare.bootcamp.week3.employeeprofile.service.impl;

import com.novare.bootcamp.week3.employeeprofile.dao.EmployeeDao;
import com.novare.bootcamp.week3.employeeprofile.model.Employee;
import com.novare.bootcamp.week3.employeeprofile.service.EmployeeService;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeDao employeeDao;

    public EmployeeServiceImpl(EmployeeDao employeeDao) {
        this.employeeDao = employeeDao;
    }

    @Override
    public Employee addEmployee(Employee employee) {
        return employeeDao.save(employee);
    }

    @Override
    public Employee getEmployee(long id) throws Exception{
        boolean isExisting = employeeDao.findById(id).isPresent();
        if(isExisting){
            return employeeDao.findById(id).get();
        }else {
            throw new Exception("Employee does not exist!");
        }

    }

    @Override
    public List<Employee> getAll() {
        return employeeDao.findAll();
    }

    @Override
    public long deleteEmployee(long id) throws Exception{
        boolean isExisting = employeeDao.findById(id).isPresent();
        if(isExisting){
            employeeDao.deleteById(id);
            return (id);
        }else {
            throw new Exception("Employee does not exist!");
        }
    }

    @Override
    public Employee updateEmployee(Employee employee) throws Exception {
        boolean isExisting = employeeDao.findById(employee.getId()).isPresent();
        if(isExisting){
            return employeeDao.save(employee);
        }else {
            throw new Exception("Employee does not exist!");
        }
    }

    @Override
    public boolean getProcess() {

        try {
            BufferedReader br = new BufferedReader(new FileReader("test.csv"));
            String line;
            br.readLine();

            while((line = br.readLine()) != null) {
                String[] values = line.split(",");
//                Date date = new SimpleDateFormat("yy/MM/dd").parse(values[2]);
                Employee employee = new Employee(values[0], values[1], values[2], values[3]);
                employeeDao.save(employee);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

}
