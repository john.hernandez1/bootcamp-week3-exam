package com.novare.bootcamp.week3.employeeprofile.service;

import com.novare.bootcamp.week3.employeeprofile.model.Manager;

public interface ManagerService {
    Manager addManager(Manager manager);

    Manager getManager(long id);

    Manager getAll();

    void updateManager(Manager manager);

    void deleteManager(long id);
}

