package com.novare.bootcamp.week3.employeeprofile.bdd;


import com.novare.bootcamp.week3.employeeprofile.dao.EmployeeDao;
import com.novare.bootcamp.week3.employeeprofile.model.Employee;
import com.novare.bootcamp.week3.employeeprofile.service.impl.EmployeeServiceImpl;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class EmployeeControllerCucumberTest {

    EmployeeDao employeeDao = mock(EmployeeDao.class);
    EmployeeServiceImpl employeeService = new EmployeeServiceImpl(employeeDao);

    Employee newEmployee;

    Employee employeeData = new Employee(1, "Test", "Cucumber", "IntelliJ", "Manager" );
    List<Employee> employeeList = Arrays.asList(newEmployee,
            new Employee(2, "Test2", "Cucumber2", "IntelliJ2", "Secretary" ));

    @Given(value="Employee has Id")
    public void employeeHasName() {
        employeeData.getId();
        System.out.println("Employee has Id");
    }

    @Given(value="Employee has firstName")
    public void employeeHasFirstName() {
        employeeData.getFirstName();
        System.out.println("Employee has FirstName");
    }

    @Given(value="Employee has lastName")
    public void employeeHasLastName() {
        employeeData.getLastName();
        System.out.println("Employee has LastName");
    }

    @Given(value="Employee has department")
    public void emplopeeHasDepartment() {
        employeeData.getDepartment();
        System.out.println("Employee has department");
    }

    @Given(value="Employee has position")
    public void employeeHasPosition() {
        employeeData.getPosition();
        System.out.println("Employee has position");
    }

    @When(value="I create employee add data")
    public void createEmployeeData() {
        newEmployee = employeeData;
        System.out.println("Created new employee data");
    }

    @Then(value="Add employee data")
    public void addEmployeeData() {
        assertTrue(newEmployee.isValid());
        System.out.println("Employee added");
    }

    @When(value="Employee to update is existing")
    public void updateEmployeeIsExisting() throws Exception{
        when(employeeDao.findById(employeeData.getId())).thenReturn(Optional.of(employeeData));
        assertTrue(employeeDao.findById(employeeData.getId()).isPresent());
        System.out.println("Employee to update is existing");
    }

    @When(value="I create employee update data")
    public void createUpdateEmployeeData() {
        newEmployee = new Employee(1, "Testing", "Cucumbering", "Fruits", "Go foods");
        System.out.println("I create employee update data");
    }

    @Then(value="Update employee data")
    public void updateEmployeeData() throws Exception {
        when(employeeDao.findById(employeeData.getId())).thenReturn(Optional.of(employeeData));
        when(employeeDao.save(employeeData)).thenReturn(employeeData);
        assertEquals(employeeData, employeeService.updateEmployee(employeeData));
        System.out.println("Employee was updated");
    }

    @When(value="Employee to delete is existing")
    public void deleteEmployeeIsExisting() {
        when(employeeDao.findById(employeeData.getId())).thenReturn(Optional.of(employeeData));
        assertTrue(employeeDao.findById(employeeData.getId()).isPresent());
        System.out.println("Employee to delete is existing");
    }

    @Then(value="Delete employee data")
    public void deleteEmployeeData() throws Exception {
        when(employeeDao.findById(employeeData.getId())).thenReturn(Optional.of(employeeData));
        assertEquals(employeeData.getId(), employeeService.deleteEmployee(employeeData.getId()));
        System.out.println("Employee deleted");
    }

    @When(value="Employee to read is existing")
    public void readEmployeeIsExisting() {
        when(employeeDao.findAll()).thenReturn(employeeList);
        System.out.println("Employee exist");
    }

    @Then(value="Return all data")
    public void returnAllEmployeeData() {
        when(employeeDao.findAll()).thenReturn(employeeList);
        assertEquals(employeeList, employeeService.getAll());
        System.out.println("All Employee return");
    }

    @When(value="Employee to read by Id is existing")
    public void readIdEmployeeIsExisting() {
        when(employeeDao.findById(employeeData.getId())).thenReturn(Optional.of(employeeData));
        assertTrue(employeeDao.findById(employeeData.getId()).isPresent());
        System.out.println("Employee exist");
    }

    @Then(value="Return employee by Id")
    public void returnByIdEmployeeData() throws Exception {
        when(employeeDao.findById(employeeData.getId())).thenReturn(Optional.of(employeeData));
        assertEquals(employeeData, employeeService.getEmployee(employeeData.getId()));
        System.out.println("Employee returned");
    }

}
