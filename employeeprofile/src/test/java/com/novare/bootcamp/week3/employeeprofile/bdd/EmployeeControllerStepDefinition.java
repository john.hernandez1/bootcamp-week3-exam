package com.novare.bootcamp.week3.employeeprofile.bdd;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features="classpath:EmployeeController.feature", glue="com.novare.bootcamp.week3.employeeprofile.bdd")
public class EmployeeControllerStepDefinition {
}
