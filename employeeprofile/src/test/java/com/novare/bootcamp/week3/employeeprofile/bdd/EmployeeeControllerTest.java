package com.novare.bootcamp.week3.employeeprofile.bdd;

import com.novare.bootcamp.week3.employeeprofile.dao.EmployeeDao;
import com.novare.bootcamp.week3.employeeprofile.model.Employee;
import com.novare.bootcamp.week3.employeeprofile.service.impl.EmployeeServiceImpl;
import org.junit.Test;
import org.junit.internal.runners.JUnit4ClassRunner;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class EmployeeeControllerTest {

//    @InjectMocks
//    EmployeeServiceImpl employeeService;

//    @Mock
//    EmployeeDao employeeDao;

    EmployeeDao employeeDao = mock(EmployeeDao.class);
    EmployeeServiceImpl employeeService = new EmployeeServiceImpl(employeeDao);

    Employee newEmployee;

    @Test
    @DisplayName("Employee has firstName, lastName, department and position")
    public void test1() {
        newEmployee = new Employee("test", "cucumber", "fruits", "go foods");
        assertTrue(newEmployee.isValid());
    }

    @Test
    @DisplayName("Employee create")
    public void test2() {
        newEmployee = new Employee("test", "cucumber", "fruits", "go foods");
        when(employeeDao.save(newEmployee)).thenReturn(newEmployee);
        assertEquals(newEmployee, employeeService.addEmployee(newEmployee));
    }

    @Test
    @DisplayName("Employee update")
    public void test3() throws Exception{
        newEmployee = new Employee(1, "test", "cucumber", "fruits", "go foods");
        when(employeeDao.findById(newEmployee.getId())).thenReturn(Optional.of(newEmployee));
        when(employeeDao.save(newEmployee)).thenReturn(newEmployee);
        assertEquals(newEmployee, employeeService.updateEmployee(newEmployee));
    }

    @Test
    @DisplayName("Employee deleted")
    public void test4() throws Exception{
        newEmployee = new Employee(1, "test", "cucumber", "fruits", "go foods");
        when(employeeDao.findById(newEmployee.getId())).thenReturn(Optional.of(newEmployee));
        assertEquals(newEmployee.getId(), employeeService.deleteEmployee(newEmployee.getId()));
    }

    @Test
    @DisplayName("Employee via Id")
    public void test5() throws Exception {
        newEmployee = new Employee(1, "test", "cucumber", "fruits", "go foods");
        when(employeeDao.findById(newEmployee.getId())).thenReturn(Optional.of(newEmployee));
        assertEquals(newEmployee, employeeService.getEmployee(newEmployee.getId()));
    }

    @Test
    @DisplayName("All employee")
    public void test6() {
        List<Employee> employeeList = Arrays.asList(new Employee("test", "cucumber", "fruits", "go foods"),
                new Employee("test2", "cucumber2", "fruits2", "go foods2"));

        when(employeeDao.findAll()).thenReturn(employeeList);
        assertEquals(employeeList, employeeService.getAll());
    }

}
