package com.novare.bootcamp.week3.employeeprofile.service.impl;

import com.novare.bootcamp.week3.employeeprofile.dao.EmployeeDao;
import com.novare.bootcamp.week3.employeeprofile.model.Employee;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
@ExtendWith(MockitoExtension.class)
class EmployeeServiceImplTest {

    @InjectMocks
    EmployeeServiceImpl employeeService;

    @Mock
    EmployeeDao employeeRepository;
    Employee testEmployee;
    List<Employee> testEmployeeList;

    @BeforeEach
    void init(){
        testEmployee = new Employee("john","louie","SEG","ASE");
        testEmployeeList = List.of(new Employee("kim","rafallo","SEG","ASE"),
                               new Employee("kenneth","ramos","SEG","ASE"));
    }
    @Test
    @DisplayName("I want to add employee")
    void addEmployee() {
        Mockito.when(employeeRepository.save(testEmployee)).thenReturn(testEmployee);
        Assertions.assertEquals(testEmployee,employeeService.addEmployee(testEmployee));
    }

    @Test
    @DisplayName("I want to find an employee by id")
    void getEmployee() throws Exception {
         Mockito.when(employeeRepository.findById(testEmployee.getId())).thenReturn(Optional.of(testEmployee));
         Assertions.assertEquals(testEmployee,employeeService.getEmployee(testEmployee.getId()));
    }
    @Test
    @DisplayName("I want an Exception if id doesnt exist")
    void getEmployeeFail(){
        try{
            Mockito.when(employeeRepository.findById(testEmployee.getId())).thenThrow(new Exception());
        } catch (Exception e) {
            Assertions.assertThrows(Exception.class,()->employeeService.getEmployee(testEmployee.getId()));
        }
    }

    @Test
    @DisplayName("I want to find all employee")
    void getAll() {
        Mockito.when(employeeRepository.findAll()).thenReturn(testEmployeeList);
        Assertions.assertEquals(testEmployeeList,employeeService.getAll());
    }

    @Test
    @DisplayName("I want to delete an employee by its id")
    void deleteEmployee() throws Exception {
        Mockito.when(employeeRepository.findById(testEmployee.getId())).thenReturn(Optional.ofNullable(testEmployee));
        Assertions.assertEquals(testEmployee.getId(), employeeService.deleteEmployee(testEmployee.getId()));
    }
    @Test
    @DisplayName("I want to get an Exception when i cant find the employee")
    void deleteEmployeeFail() throws Exception{
        try{
            Mockito.when(employeeRepository.findById(testEmployee.getId())).thenThrow(new Exception());
        } catch (Exception e) {
            Assertions.assertThrows(Exception.class,()->employeeService.deleteEmployee(testEmployee.getId()));
        }
    }

    @Test
    @DisplayName("I want to find an employee by its id then update it")
    void updateEmployee() throws Exception{
        Mockito.when(employeeRepository.findById(testEmployee.getId())).thenReturn(Optional.ofNullable(testEmployee));
        Mockito.when(employeeRepository.save(testEmployee)).thenReturn(testEmployee);
        Assertions.assertEquals(testEmployee,employeeService.updateEmployee(testEmployee));
    }
    @Test
    @DisplayName("I want to get an Exception when i cant find the employee")
    void updateEmployeeFail(){
        Employee nonExistingEmployee = new Employee("paul","hern","SEG","ASE");
        try{
            Mockito.when(employeeRepository.findById(testEmployee.getId()).isPresent()).thenThrow(new Exception());
        } catch (Exception e) {
            Assertions.assertThrows(Exception.class,()->employeeService.updateEmployee(nonExistingEmployee));
        }
    }
}